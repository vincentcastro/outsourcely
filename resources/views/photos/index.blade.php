@extends('master.layout')
@extends('master.nav')
@extends('master.sidebar')

@section('title', 'Photos')

@section('tophead')
@endsection

@section('bottomhead')

<script type="text/javascript">

		var data = <?=$tags?>;

		var tags = new Bloodhound({
		    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		    queryTokenizer: Bloodhound.tokenizers.whitespace,
		    local: $.map(data, function (tag) {
		        return {
		            name: tag
		        };
		    })
		});
		tags.initialize();

		$('#tags,#search').tagsinput({
		  	typeaheadjs: [{
		        minLength: 1,
		        highlight: true,
			},{
			    minlength: 1,
		        name: 'tags',
		        displayKey: 'name',
		        valueKey: 'name',
			    source: tags.ttAdapter()
			}]
		});

</script>
@endsection

@section('content')

<div class="row content">
	<div class="col-md-6">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"> Create Photos </button>
	</div>
	<div class="col-md-6">
		<form class="form-inline float-right" method="GET">
		  <div class="form-group mx-sm-3 mb-2">
		    <input type="text" class="form-control" name="s" id="search" placeholder="Search Photos by Tags">
		  </div>
		  <button type="submit" class="btn btn-primary mb-2">Search</button>
		  <a href="{{route('home')}}" class="btn btn-primary mb-2">Clear</a>
		</form>
	</div>
	<div class="col-md-12 content">
		<div class="row">
			@foreach($photos as $photo)
			<div class="col-md-2">
              <div class="card mb-4 shadow-sm">
                <img class="card-img-top" data-src="{{ url($photo->file_path) }}" alt="{{$photo->name}}" style="height: 225px; width: 100%; display: block;" src="{{ url($photo->file_path) }}" data-holder-rendered="true">
                <div class="card-body">
                	<h6>Tags: </h6>
                	@foreach($photo->tags as $tags)
                		<span class="tag label label-info" style="margin-left: 5px"> {{ $tags->name }} </span>
                  	@endforeach
                </div>
              </div>
            </div>
            @endforeach
		</div>
		
	</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <form method="POST" action="{{ route('photo.store') }}" enctype="multipart/form-data"> @csrf
      <div class="modal-header">
        <h5 class="modal-title">Upload Photo</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
		  <div class="form-group row">
		    <label class="col-sm-2 col-form-label">Photo</label>
		    <div class="col-sm-10">
		      <input type="file" class="form-control" id="photo" name="photo">
		    </div>
		  </div>
        	<div class="alert alert-info" role="alert">
			  Tags can be separated by comma "," or enter.
			</div>
		  <div class="form-group row">
		    <label class="col-sm-2 col-form-label">Tags</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="tags" name="tags">
		    </div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
	  </form>
    </div>
  </div>
</div>

@endsection
