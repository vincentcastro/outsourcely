@extends('master.layout')
@extends('master.nav')
@extends('master.sidebar')

@section('title', 'Tags')

@section('tophead')
@endsection

@section('bottomhead')
@endsection

@section('content')

<div class="row content">
	<div class="col-md-12">
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"> Create Tags </button>
	</div>
	<div class="col-md-12 content">
		<table class="table">
			<thead>
			    <tr>
			      <th scope="col">#</th>
			      <th scope="col">Name</th>
			      <th scope="col">Slug</th>
			      <th scope="col">Created Date</th>
			    </tr>
			  </thead>
			  <tbody>
			  	@foreach($tags as $tag)
			    <tr>
			      <th scope="row">{{ $tag->tag_id }}</th>
			      <td>{{ $tag->name }}</td>
			      <td>{{ $tag->slug }}</td>
			      <td>{{ $tag->created_at }}</td>
			    </tr>
			    @endforeach
			  </tbody>
		</table>
	</div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form method="POST" action="{{ route('tag.store') }}"> @csrf
      <div class="modal-header">
        <h5 class="modal-title">Create Tag</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        	<div class="alert alert-info" role="alert">
			  Tags can be separated by comma "," or enter.
			</div>
		  <div class="form-group row">
		    <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" data-role="tagsinput" id="name" name="tags">
		    </div>
		  </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
	  </form>
    </div>
  </div>
</div>

@endsection
