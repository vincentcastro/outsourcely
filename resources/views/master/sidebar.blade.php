@section('sidebar')
<nav class="col-md-2 d-none d-md-block bg-light sidebar">
  <div class="sidebar-sticky">
    <ul class="nav flex-column">
      <li class="nav-item">
        <a class="nav-link" href="{{ route('home') }}">
          Photos
        </a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="{{ route('tag') }}">
          Tags
        </a>
      </li>
    </ul>
  </div>
</nav>
@endsection