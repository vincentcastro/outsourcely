<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Photos extends Model
{
    protected $table = 'photos';
	
    protected $fillable = [
				            'photo_id', 
				            'name', 
				            'file_path'
						];
							
    protected $primaryKey = 'photo_id';

    /**
     * The users that belong to the role.
     */
    public function tags()
    {
        return $this->belongsToMany('App\Tags', 'photo_tag', 'photo_id', 'tag_id');
    }
}
