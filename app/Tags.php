<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    protected $table = 'tags';
	
    protected $fillable = [
				            'tag_id', 
				            'name', 
				            'slug'
						];
							
    protected $primaryKey = 'tag_id';

    /**
     * The users that belong to the role.
     */
    public function photos()
    {
        return $this->belongsToMany('App\Photos');
    }

    public static function checkTagExist($slug)
    {
        return static::where('slug',$slug)->first();
    }
}
