<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Tags;

class TagsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'tags' => Tags::get()
        ];
        return view('tags.index',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $tags = $data['tags'];

        if(!empty($tags)){
            $splitTags = explode(',', $tags);
            foreach ($splitTags as $key => $value) {
                $createData = [
                    'name' => $value,
                    'slug' => strtolower(str_replace(" ","-",$value))
                ];
                $exist = Tags::checkTagExist($createData['slug']);
                if(!$exist){
                    $createTag = Tags::create($createData);
                }
            }
        }

        $data = [
            'tags' => Tags::get()
        ];
        return redirect()->route('tag',$data);
    }
}
