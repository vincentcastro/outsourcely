<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Photos;
use App\Tags;

class PhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();
        $returnTags = array();
        $tags = Tags::select('name')->get();

        if(isset($data['s'])){ // SEARCH
            $tagsID = array();
            $splitTags = explode(',', $data['s']);
            foreach ($splitTags as $key => $value) {
                $exist = Tags::checkTagExist( strtolower(str_replace(" ","-",$value)) );
                if($exist){ // GET ALL MATCHING TAG IDS
                    $tagsID[] = $exist->tag_id;
                }
            }
            $photos= Photos::whereHas('tags', function($q) use ($tagsID){
                           $q->whereIn('tags.tag_id',$tagsID);
                    })->get();
        } else { // GET ALL PHOTOS
            $photos = Photos::get();
        }
        foreach ($tags as $tag) {
            $returnTags[] = $tag->name;
        }
        $data = [
            'tags' => json_encode($returnTags),
            'photos' => $photos
        ];
        return view('photos.index',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();

        $tags = $data['tags'];
        
        //Create image
        $path = $request->file('photo')->store('photos', ['disk' => 'photo']);

        $photoData = [
            'name' => $request->file('photo')->getClientOriginalName(),
            'file_path' => $path
        ];

        $createPhoto = Photos::create($photoData);

        if(!empty($tags)){
            $tagsID = array();
            $splitTags = explode(',', $tags);
            foreach ($splitTags as $key => $value) {
                $createData = [
                    'name' => $value,
                    'slug' => strtolower(str_replace(" ","-",$value))
                ];
                $exist = Tags::checkTagExist($createData['slug']);
                if(!$exist){ // GET TAG ID
                    $createTag = Tags::create($createData);
                    $tagID = $createTag->tag_id;
                } else { // CREATE IF TAG DOESNT EXIST
                    $tagID = $exist->tag_id;
                }

                $tagsID[] = $tagID;
            }
            $createPhoto->tags()->sync($tagsID);
        }     

        return redirect()->route('home');
    }
}
