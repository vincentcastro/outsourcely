<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});
*/
Route::get('/', 'PhotosController@index')->name('home');

Route::group(['prefix' => 'photo'], function () {
	Route::post('/store', 'PhotosController@store')->name('photo.store');
});

Route::group(['prefix' => 'tag'], function () {
	Route::get('/', 'TagsController@index')->name('tag');
	Route::post('/store', 'TagsController@store')->name('tag.store');
});